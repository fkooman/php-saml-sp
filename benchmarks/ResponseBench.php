<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

use fkooman\SAML\SP\CryptoKeys;
use fkooman\SAML\SP\IdpInfo;
use fkooman\SAML\SP\Log\NullLogger;
use fkooman\SAML\SP\PublicKey;
use fkooman\SAML\SP\Response;
use fkooman\SAML\SP\SpInfo;

class ResponseBench
{
    /**
     * @Revs(1000)
     *
     * @Iterations(5)
     *
     * @OutputTimeUnit("seconds")
     *
     * @OutputMode("throughput")
     */
    public function benchHandleResponse(): void
    {
        $response = new Response(new DateTimeImmutable('2019-02-23T17:04:21Z'), new NullLogger());
        $samlResponse = file_get_contents(\dirname(__DIR__) . '/tests/data/assertion/FrkoIdP.xml');
        $response->verify(
            new SpInfo(
                'http://localhost:8081/metadata',
                CryptoKeys::load(\dirname(__DIR__) . '/tests/data/certs'),
                'http://localhost:8081/acs',
                'http://localhost:8081/slo',
                false,
                ['en-US' => 'My SP', 'nl-NL' => 'Mijn SP'],
                '{{USER_ID}}'
            ),
            new IdpInfo('http://localhost:8080/metadata.php', 'Test', 'http://localhost:8080/sso.php', null, [PublicKey::fromFile(\dirname(__DIR__) . '/tests/data/certs/FrkoIdP.crt')], []),
            $samlResponse,
            '_2483d0b8847ccaa5edf203dad685f860',
            [],
            []
        );
    }
}
