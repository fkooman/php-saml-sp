# API

An API is provided for easy integration of SAML authentication in your PHP
application. See the `example/` directory for a working example.

The rest of this document will tell you what other options you have.

Three classes are relevant: `SamlAuth` (`src/Api/SamlAuth.php`), `AuthOptions` 
(`src/Api/AuthOptions.php`) and `Assertion` (`src/Assertion.php`). You can take
a look at them if you'd like, they won't bite.

See the [Composer](#composer) section on how to add php-saml-sp as a 
development dependency to your project.

## AuthOptions

You can configure the SAML authentication flow by initializing the 
`AuthOptions` object and setting some parameters. The following methods are 
defined in `AuthOptions`. All of them are *OPTIONAL*:

* `withReturnTo($returnTo)`: if you want to return to a 
  specific URL after the authentication is complete. By default you will return 
  to the URL where you started the authentication from;
* `withAuthnContextClassRef(array $authnContextClassRef)`: if 
  you want to request a certain 
  [Authentication Context](https://docs.oasis-open.org/security/saml/v2.0/saml-authn-context-2.0-os.pdf), 
  e.g. for Two Factor authentication;
* `withIdp($idpEntityId)`: if you want to send the browser to a 
  specific IdP (by its `entityID` directly instead of showing the "WAYF" in 
  case there are multiple IdPs linked to this SP;
* `withScopingIdpList(array $scopingIdpList)`: if your SP is
  behind a proxy server and you want to restrict, or select the IdP(s) the user 
  is allowed to choose in the proxy IdP selector.
  
### Example

```php
<?php

use fkooman\SAML\SP\Api\AuthOptions;

$authOptions = AuthOptions::init()
    ->withReturnTo('https://sp.example.org/account')
    ->withAuthnContextClassRef(['urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken'])
    ->withIdP('https://proxy.example.com/saml')
    ->withScopingIdpList(
        ['https://idp.example.org/saml', 'https://idp.example.com/saml']
    );
```
## SamlAuth

To actually use/trigger SAML authentication, you can use the `SamlAuth` class, 
the following methods are defined:

* `getLoginURL(?AuthOptions $authOptions = null) : string`: get the URL your 
  application needs to redirect to in order to trigger authentication;
* `getAssertion(?AuthOptions $authOptions = null) : Assertion`: get the SAML 
  assertion object if (and only if) the user is already authenticated;
* `isAuthenticated(?AuthOptions $authOptions = null) : bool`: figure out 
  whether the user is authenticated already;
* `getAndVerifyAssertion(?AuthOptions $authOptions = null) ?Assertion`: get 
  the SAML assertion when the user was successfully authenticated, or `null` 
  otherwise;
* `getLogoutURL(string $returnTo): string`: get the URL your application needs 
  to redirect to in order to trigger logout.

### Example

This is a complete example that does not set any `AuthOptions`, but you can 
easily add them by following the previous example, if needed. Once you 
installed php-saml-sp, the file below can be put in `/var/www/html/saml.php`. 
Visit it using your browser at `http://host/saml.php` and authentication should 
be triggered.

```php
<?php

declare(strict_types=1);

require_once '/usr/share/php/fkooman/SAML/SP/autoload.php';

use fkooman\SAML\SP\Api\AuthOptions;
use fkooman\SAML\SP\Api\SamlAuth;

$authOptions = AuthOptions::init();
$samlAuth = new SamlAuth();
if (!$samlAuth->isAuthenticated($authOptions)) {
    header('Location: ' . $samlAuth->getLoginURL($authOptions));
    exit(0);
}
$samlAssertion = $samlAuth->getAssertion($authOptions);
echo htmlentities($samlAssertion->getIssuer()) . '<br>';
if (null !== $nameId = $samlAssertion->getNameId()) {
    echo htmlentities($nameId->toXml()) . '<br>';
}
foreach ($samlAssertion->getAttributes() as $k => $v) {
    echo htmlentities($k . ': ' . \implode(',', $v)) . '<br>';
}
```

## Assertion

The `getAssertion` returns an `Assertion` object, which has the following 
methods that can be used to get more information about the authenticated 
session:

* `getIssuer() : string`: get the issuer of the SAML assertion;
* `getNameId() : ?NameId`: get the `NameID` object, see `src/NameID.php`;
* `getAuthnInstant() : DateTime`: get the moment the authentication took place;
* `getSessionNotOnOrAfter() : DateTime`: get the time until which the assertion 
  is valid;
* `getAuthnContext() : string`: get the authentication context (see 
  `AuthOptions` above that was actually granted;
* `getAuthenticatingAuthority() : ?string`: get the authenticating IdP, in case 
  a SAML proxy was used;
* `getAttributes() : array<string,array<string>>`: get the list of attributes 
  received from the IdP. This is an `array` with the attribute name as "key" 
  and the attribute value(s) as an `array` of `string`;
* `getAttributeValues(string $attributeName): ?array`: get the attribute 
  value(s) of one particular attribute specified by name. Returns `null` if the
  attribute does NOT exist;
* `getFirstAttributeValue(string $attributeName): ?string`: get the *first* 
  attribute value of an attribute specified by name. Returns `null` if the 
  attribute does NOT exist.
  
# Composer

If you are using [Composer](https://getcomposer.org/) for dependency management
in your application, you can add php-saml-sp as a development dependency in
order to do e.g. static code analysis to make sure your integration works 
properly.

Add the following in the "root" of your `composer.json`. The projects are NOT 
registered in [Packagist](https://packagist.org/) and therefore we need to add
the repositories manually.

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://codeberg.org/fkooman/php-secookie"
    },
    {
        "type": "vcs",
        "url": "https://codeberg.org/fkooman/php-saml-sp"
    }
],
"require-dev": {
    "fkooman/saml-sp": "^2"
},
```

Make sure you merge these changes carefully and not break JSON. Use 
`composer validate` after making modifications, and then run `composer update`.

You can then use the Composer autoloader during development and run your static
code analysis using e.g. [Psalm](https://psalm.dev/).

For _runtime_ it is recommend to use the global php-saml-sp autoloader as shown 
in the example in the `example/` directory:

```
// when using Debian/Fedora/CentOS/RHEL package
require_once '/usr/share/php/fkooman/SAML/SP/autoload.php';
```
