<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

return [
    'Assertion' => 'Billet',
    'Attributes' => 'Attributter',
    'Authentication' => 'Autentifikation',
    'Error' => 'Fejl',
    'Error Message' => 'Fejlmeddelelse',
    'IdPs ❤️ SP metadata! Use the URL or the XML below to feed your IdP.' => 'IdP\'er ❤️ SP-metadata! Brug URL\'en eller XML\'en nedenfor som foder til din IdP.',
    'Issuer' => 'Udsteder',
    'Logout' => 'Log ud',
    'Metadata' => 'Metadata',
    'No IdP(s) configured for authenticating to this SP!' => 'Ingen IdP(\'er) sat op til autentifikation for den her SP!',
    'No Results!' => 'Ingen resultater!',
    'Other...' => 'Andre ...',
    'Search for your organization...' => 'Søg efter din organisation ...',
    'Select your organization to continue the login process.' => 'Vælg din organisation for at fortsætte loginprocessen.',
    //'Switch' => '',
    'Test' => 'Test',
    'The IdP provided no attributes, or they were not in a supported format.' => 'IdP\'en sendte ingen attributter i noget understøttet format.',
    'This is the information page of this SAML SP. If you don\'t know what SAML is, you should not have arrived here! 🤔' => 'Det her er oplysningssiden for den her SAML-SP. Hvis du ikke ved hvad SAML er, skulle du slet ikke være havnet her! 🤔',
    'Welcome' => 'Velkommen',
    'XML' => 'XML',
    'You can perform authentication tests here with the configured IdP(s).' => 'Her kan man teste autentifikation med de IdP\'er der er sat op.',
];
