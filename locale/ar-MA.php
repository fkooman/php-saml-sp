<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

return [
    'Assertion' => 'إقرار',
    'Attributes' => 'سمات',
    'Authentication' => 'مصادقة',
    'Error' => 'خطأ',
    'Error Message' => 'رسالة الخطأ',
    'IdPs ❤️ SP metadata! Use the URL or the XML below to feed your IdP.' => 'مزودوا الهوية ❤️  البيانات الوصفية للخدمة! استخدم الرابط أو XML أدناه لتغذية مزود الهوية',
    'Issuer' => 'الجهة المصدرة',
    'Logout' => 'تسجيل الخروج',
    'Metadata' => 'البيانات الوصفية',
    'No IdP(s) configured for authenticating to this SP!' => 'لم يتم اعداد اي مزود هوية للمصادقة على هذه الخدمة!',
    'No Results!' => 'لا نتائج!',
    'Other...' => 'آخر',
    'Search for your organization...' => 'ابحث عن مؤسستك',
    'Select your organization to continue the login process.' => 'حدد مؤسستك لمواصلة عملية تسجيل الدخول.',
    'Switch' => 'بدل',
    'Test' => 'جرب',
    'The IdP provided no attributes, or they were not in a supported format.' => 'لم يقدم مزود الهوية أي سمات، أو لم تكن بتنسيق مدعوم.',
    'This is the information page of this SAML SP. If you don\'t know what SAML is, you should not have arrived here! 🤔' => 'هذه هي صفحة معلومات SAML لهذه الخدمة. إذا كنت لا تعرف ما هو SAML، فلا ينبغي لك أن تصل إلى هنا! 🤔',
    'Welcome' => 'مرحبا',
    'XML' => 'XML',
    'You can perform authentication tests here with the configured IdP(s).' => 'يمكنك إجراء اختبارات المصادقة هنا باستخدام مزود(ي) الهوية المعد(ة).',
];
