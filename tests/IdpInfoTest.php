<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Tests;

use fkooman\SAML\SP\IdpInfo;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class IdpInfoTest extends TestCase
{
    public function testGetDisplayNameFromMetadata(): void
    {
        $idpInfo = IdpInfo::fromXml(file_get_contents(__DIR__ . '/data/metadata/idp.tuxed.net.xml'));
        $this->assertSame('FrkoIdP', $idpInfo->getDisplayName());
        $this->assertSame('FrkoIdP', $idpInfo->getDisplayName('en'));
        $this->assertSame('FrkoIdP (NL)', $idpInfo->getDisplayName('nl'));
    }

    public function testDisplayNames(): void
    {
        $idpInfo = new IdpInfo('', ['en' => 'EN', 'nl-NL' => 'nl-NL', 'nl' => 'NL'], '', null, [], []);
        $this->assertSame('EN', $idpInfo->getDisplayName());
        $this->assertSame('EN', $idpInfo->getDisplayName('en'));
        $this->assertSame('nl-NL', $idpInfo->getDisplayName('nl-NL'));
        $this->assertSame('NL', $idpInfo->getDisplayName('nl'));
        $this->assertSame('NL', $idpInfo->getDisplayName('nl-XYZ'));
    }
}
