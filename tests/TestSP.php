<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Tests;

use DateTimeImmutable;
use fkooman\SAML\SP\AttributeMapping;
use fkooman\SAML\SP\IdpSourceInterface;
use fkooman\SAML\SP\Log\NullLogger;
use fkooman\SAML\SP\SessionInterface;
use fkooman\SAML\SP\SP;
use fkooman\SAML\SP\SpInfo;

class TestSP extends SP
{
    public function __construct(SpInfo $spInfo, IdpSourceInterface $idpSource, SessionInterface $session)
    {
        parent::__construct($spInfo, $idpSource, $session, new NullLogger(), new AttributeMapping());
    }

    public function setDateTime(DateTimeImmutable $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    public function setSession(SessionInterface $session): void
    {
        $this->session = $session;
    }

    protected function requestId(): string
    {
        return '0123456789abcdef';
    }

    protected function relayState(): string
    {
        return '1234_relay_state_5678';
    }
}
