<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Tests;

use fkooman\SAML\SP\AttributeMapping;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class AttributeMappingTest extends TestCase
{
    public function testUid(): void
    {
        $attributeMapping = new AttributeMapping();
        $this->assertSame('uid', $attributeMapping->get('urn:oid:0.9.2342.19200300.100.1.1'));
    }

    public function testMissing(): void
    {
        $attributeMapping = new AttributeMapping();
        $this->assertNull($attributeMapping->get('foo_bar_baz'));
    }

    public function testCustom(): void
    {
        $attributeMapping = new AttributeMapping(['foo_bar_baz' => 'baz']);
        $this->assertSame('baz', $attributeMapping->get('foo_bar_baz'));
    }

    public function testOverride(): void
    {
        // custom entries ALWAYS override the "embedded" ones
        $attributeMapping = new AttributeMapping(['urn:oid:0.9.2342.19200300.100.1.1' => 'my-uid']);
        $this->assertSame('my-uid', $attributeMapping->get('urn:oid:0.9.2342.19200300.100.1.1'));
    }

    public function testJsonFile(): void
    {
        $attributeMapping = AttributeMapping::fromJsonFile(__DIR__ . '/data/custom_mapping.json');
        $this->assertSame('bar', $attributeMapping->get('foo'));
    }
}
