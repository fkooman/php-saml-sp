<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Api\Tests;

use fkooman\SAML\SP\Tests\TestSamlAuth;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class SamlAuthTest extends TestCase
{
    public function testGetLoginURL(): void
    {
        $s = new TestSamlAuth();
        $this->assertSame('https://service.example.org/php-saml-sp/login?ReturnTo=https%3A%2F%2Fservice.example.org%2Fapp', $s->getLoginURL());
    }

    public function testGetLogoutURLAbsolute(): void
    {
        $s = new TestSamlAuth();
        $this->assertSame('https://service.example.org/php-saml-sp/logout?ReturnTo=https%3A%2F%2Fwww.php-saml-sp.eu%2F', $s->getLogoutURL('https://www.php-saml-sp.eu/'));
    }

    public function testGetLogoutURLRelative(): void
    {
        $s = new TestSamlAuth();
        $this->assertSame('https://service.example.org/php-saml-sp/logout?ReturnTo=https%3A%2F%2Fservice.example.org%2F', $s->getLogoutURL('/'));
    }
}
