<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Api\Tests;

use fkooman\SAML\SP\Api\AuthOptions;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class AuthOptionsTest extends TestCase
{
    public function testEmptyAuthnContextClassRef(): void
    {
        $authOptions = AuthOptions::init();
        $this->assertSame([], $authOptions->getAuthnContextClassRef());
    }

    public function testAuthnContextClassRef(): void
    {
        // I got super scared here! The "withAuthnContextClassRef" ALSO updates
        // the object itself, it is mutable so it does NOT only return a new
        // object that has AuthnContextClassRef set.
        $authOptions = AuthOptions::init();
        $authOptions->withAuthnContextClassRef(['foo', 'bar']);
        $this->assertSame(['foo', 'bar'], $authOptions->getAuthnContextClassRef());
    }

    public function testChainedAuthnContextClassRef(): void
    {
        $authOptions = AuthOptions::init()->withAuthnContextClassRef(['foo', 'bar']);
        $this->assertSame(['foo', 'bar'], $authOptions->getAuthnContextClassRef());
    }
}
