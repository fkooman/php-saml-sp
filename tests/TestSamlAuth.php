<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Tests;

use DateTimeImmutable;
use fkooman\SAML\SP\Api\SamlAuth;
use fkooman\SAML\SP\Web\Config;
use fkooman\SAML\SP\Web\Request;

class TestSamlAuth extends SamlAuth
{
    public function __construct()
    {
        $this->config = new Config([]);
        $this->request = new Request(
            [
                'SERVER_NAME' => 'service.example.org',
                'SERVER_PORT' => '443',
                'REQUEST_SCHEME' => 'https',
                'REQUEST_URI' => '/app',
            ],
            []
        );
        $this->session = new TestSession();
        $this->dateTime = new DateTimeImmutable();
    }
}
