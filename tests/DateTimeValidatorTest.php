<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Tests;

use DateTimeImmutable;
use fkooman\SAML\SP\DateTimeValidator;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class DateTimeValidatorTest extends TestCase
{
    public function testNotBefore(): void
    {
        $this->assertFalse(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:10:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:12:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:14:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:08:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:07:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertTrue(DateTimeValidator::isBefore(new DateTimeImmutable('2018-01-01 08:06:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
    }

    public function testIsOnOrAfter(): void
    {
        $this->assertFalse(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:10:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:08:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:06:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertFalse(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:12:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertTrue(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:13:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
        $this->assertTrue(DateTimeValidator::isOnOrAfter(new DateTimeImmutable('2018-01-01 08:14:00'), new DateTimeImmutable('2018-01-01 08:10:00')));
    }
}
