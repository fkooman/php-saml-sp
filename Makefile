HTTP_PORT ?= 8082

.PHONY: test fmt psalm phpstan sloc bench dev

test:
	# try system wide installations of PHPUnit first
	/usr/bin/phpunit || /usr/bin/phpunit9 || phpunit

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	phploc src web bin

bench:
	phpbench run benchmarks

dev:
	@php -S localhost:$(HTTP_PORT) -t web
