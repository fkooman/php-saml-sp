# Memcache

It is possible to store the SAML session data in 
[memcached](https://memcached.org/) instead of on disk. This has the advantage
that you can host your application "redundantly" or for load balancing 
purposes.

## Requirements

Make sure you have the `memcached` PHP extension installed. On Fedora and Debian
the package is called `php-memcached`.

Of course, you also need one or more memcache servers that can be reached by
your server.

Make sure you restart your `php-fpm` process so the `memcached` extension is
loaded.

## Configuration

Modify `/etc/php-saml-sp/config.php`:

```php
'sessionModule' => 'memcache',
'memcacheServerList' => [
    'cache1.example.org:11211',
    'cache2.example.org:11211',
],
```

Assuming your memcache servers are available, this should immediately work.
