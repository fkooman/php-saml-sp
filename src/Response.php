<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP;

use DateInterval;
use DateTimeImmutable;
use fkooman\SAML\SP\Exception\ResponseException;
use fkooman\SAML\SP\Log\LoggerInterface;

class Response
{
    /** @var \DateTimeImmutable */
    private $dateTime;

    private LoggerInterface $logger;
    private AttributeMapping $attributeMapping;

    public function __construct(DateTimeImmutable $dateTime, LoggerInterface $logger, AttributeMapping $attributeMapping)
    {
        $this->dateTime = $dateTime;
        $this->logger = $logger;
        $this->attributeMapping = $attributeMapping;
    }

    /**
     * @param string        $samlResponse
     * @param string        $expectedInResponseTo
     * @param array<string> $authnContext
     * @param array<string> $scopingIdpList
     *
     * @throws \fkooman\SAML\SP\Exception\ResponseException
     *
     * @return Assertion
     */
    public function verify(SpInfo $spInfo, IdpInfo $idpInfo, $samlResponse, $expectedInResponseTo, array $authnContext, array $scopingIdpList)
    {
        $responseSigned = false;
        $assertionSigned = false;

        $responseDocument = XmlDocument::fromProtocolMessage($samlResponse);
        if (null !== $responseDocument->optionalOneDomElement('/samlp:Response/ds:Signature')) {
            // samlp:Response is signed
            Crypto::verifyXml($responseDocument, $idpInfo->getPublicKeys());
            $responseSigned = true;
        }

        // handle samlp:Status
        $statusCode = $responseDocument->requireOneDomAttrValue('/samlp:Response/samlp:Status/samlp:StatusCode/@Value');
        if ('urn:oasis:names:tc:SAML:2.0:status:Success' !== $statusCode) {
            $exceptionMessage = $statusCode;
            // check whether there is an additional status code
            if (null !== $additionalStatusCode = $responseDocument->optionalOneDomAttrValue('/samlp:Response/samlp:Status/samlp:StatusCode/samlp:StatusCode/@Value')) {
                $exceptionMessage = \sprintf('%s (%s)', $statusCode, $additionalStatusCode);
            }

            throw new ResponseException($exceptionMessage);
        }

        $assertionEncrypted = false;
        $assertionDocument = self::getAssertionDocument($responseDocument, $spInfo, $assertionEncrypted);

        if ($spInfo->getRequireEncryption() && !$assertionEncrypted) {
            throw new ResponseException('assertion was not encrypted, but encryption is enforced');
        }

        if (null !== $assertionDocument->optionalOneDomElement('/saml:Assertion/ds:Signature')) {
            // saml:Assertion is signed
            Crypto::verifyXml($assertionDocument, $idpInfo->getPublicKeys());
            $assertionSigned = true;
        }

        if (!$responseSigned && !$assertionSigned) {
            throw new ResponseException('samlp:Response and/or saml:Assertion MUST be signed');
        }

        // the saml:Assertion Issuer MUST be IdP entityId
        $assertionIssuer = $assertionDocument->requireOneDomElementTextContent('/saml:Assertion/saml:Issuer');
        if ($assertionIssuer !== $idpInfo->getEntityId()) {
            throw new ResponseException(\sprintf('expected saml:Issuer "%s", got "%s"', $idpInfo->getEntityId(), $assertionIssuer));
        }

        // the saml:Conditions/saml:AudienceRestriction MUST be us
        $assertionAudience = $assertionDocument->requireOneDomElementTextContent('/saml:Assertion/saml:Conditions/saml:AudienceRestriction/saml:Audience');
        if ($assertionAudience !== $spInfo->getEntityId()) {
            throw new ResponseException(\sprintf('expected saml:Audience "%s", got "%s"', $spInfo->getEntityId(), $assertionAudience));
        }

        $assertionNotOnOrAfter = new DateTimeImmutable($assertionDocument->requireOneDomAttrValue('/saml:Assertion/saml:Subject/saml:SubjectConfirmation/saml:SubjectConfirmationData/@NotOnOrAfter'));
        if (DateTimeValidator::isOnOrAfter($this->dateTime, $assertionNotOnOrAfter)) {
            throw new ResponseException('saml:Assertion no longer valid');
        }

        $assertionRecipient = $assertionDocument->requireOneDomAttrValue('/saml:Assertion/saml:Subject/saml:SubjectConfirmation/saml:SubjectConfirmationData/@Recipient');
        if ($assertionRecipient !== $spInfo->getAcsUrl()) {
            throw new ResponseException(\sprintf('expected Recipient "%s", got "%s"', $spInfo->getAcsUrl(), $assertionRecipient));
        }

        $assertionInResponseTo = $assertionDocument->requireOneDomAttrValue('/saml:Assertion/saml:Subject/saml:SubjectConfirmation/saml:SubjectConfirmationData/@InResponseTo');
        if ($assertionInResponseTo !== $expectedInResponseTo) {
            throw new ResponseException(\sprintf('expected InResponseTo "%s", got "%s"', $expectedInResponseTo, $assertionInResponseTo));
        }

        // notBefore
        $assertionNotBefore = new DateTimeImmutable($assertionDocument->requireOneDomAttrValue('/saml:Assertion/saml:Conditions/@NotBefore'));
        if (DateTimeValidator::isBefore($this->dateTime, $assertionNotBefore)) {
            throw new ResponseException('saml:Assertion is not yet valid');
        }

        $assertionAuthnInstant = new DateTimeImmutable($assertionDocument->requireOneDomAttrValue('/saml:Assertion/saml:AuthnStatement/@AuthnInstant'));

        // SessionNotOnOrAfter (Optional)
        $sessionNotOnOrAfter = $this->dateTime->add(new DateInterval('PT8H'));
        if (null !== $assertionSessionNotOnOrAfter = $assertionDocument->optionalOneDomAttrValue('/saml:Assertion/saml:AuthnStatement/@SessionNotOnOrAfter')) {
            $sessionNotOnOrAfter = new DateTimeImmutable($assertionSessionNotOnOrAfter);
        }

        $assertionAuthnContextClassRef = $assertionDocument->requireOneDomElementTextContent('/saml:Assertion/saml:AuthnStatement/saml:AuthnContext/saml:AuthnContextClassRef');
        if (0 !== \count($authnContext)) {
            // we requested a particular AuthnContext, make sure we got it
            if (!\in_array($assertionAuthnContextClassRef, $authnContext, true)) {
                throw new ResponseException(\sprintf('expected AuthnContext containing any of [%s], got "%s"', implode(',', $authnContext), $assertionAuthnContextClassRef));
            }
        }

        // AuthenticatingAuthority (Optional)
        $assertionAuthenticatingAuthority = $assertionDocument->optionalOneDomElementTextContent('/saml:Assertion/saml:AuthnStatement/saml:AuthnContext/saml:AuthenticatingAuthority');
        if (0 !== \count($scopingIdpList)) {
            // we requested a particular AuthenticatingAuthority, make sure we got it
            if (null === $assertionAuthenticatingAuthority) {
                throw new ResponseException(\sprintf('expected AuthenticatingAuthority containing any of [%s], got none', implode(',', $scopingIdpList)));
            }
            if (!\in_array($assertionAuthenticatingAuthority, $scopingIdpList, true)) {
                throw new ResponseException(\sprintf('expected AuthenticatingAuthority containing any of [%s], got "%s"', implode(',', $scopingIdpList), $assertionAuthenticatingAuthority));
            }
        }

        $attributeList = $this->extractAttributes($assertionDocument, $idpInfo, $spInfo);
        $samlAssertion = new Assertion($idpInfo->getEntityId(), $assertionAuthnInstant, $sessionNotOnOrAfter, $assertionAuthnContextClassRef, $assertionAuthenticatingAuthority, $attributeList);

        // NameID
        if (null !== $assertionNameIdDomElement = $assertionDocument->optionalOneDomElement('/saml:Assertion/saml:Subject/saml:NameID')) {
            $nameId = new NameId($idpInfo->getEntityId(), $spInfo->getEntityId(), $assertionNameIdDomElement);
            $samlAssertion->setNameId($nameId);
        }

        return $samlAssertion;
    }

    /**
     * @param bool $assertionEncrypted
     *
     * @return XmlDocument
     */
    private static function getAssertionDocument(XmlDocument $responseDocument, SpInfo $spInfo, &$assertionEncrypted)
    {
        if (null !== $responseDocument->optionalOneDomElement('/samlp:Response/saml:EncryptedAssertion')) {
            $decryptedAssertion = Crypto::decryptXml($responseDocument, $spInfo->getCryptoKeys()->getEncryptionPrivateKey());
            $assertionEncrypted = true;

            // create and validate new document for Assertion
            return XmlDocument::fromProtocolMessage($decryptedAssertion);
        }

        $assertionElement = $responseDocument->requireOneDomElement('/samlp:Response/saml:Assertion');

        return XmlDocument::fromProtocolMessage(XmlDocument::domElementToString($assertionElement));
    }

    /**
     * @return array<string,array<string>>
     */
    private function extractAttributes(XmlDocument $assertionDocument, IdpInfo $idpInfo, SpInfo $spInfo)
    {
        $attributeList = [];
        foreach ($assertionDocument->allDomAttrValue('/saml:Assertion/saml:AttributeStatement/saml:Attribute/@Name') as $attributeName) {
            if (null === $friendlyName = $this->attributeMapping->get($attributeName)) {
                // we only process attributes we know about, ignore the rest
                $this->logger->warning(
                    \sprintf(
                        'attribute "%s" not processed as it is not in "urn:oid" format, or we do not recognize it',
                        $attributeName
                    )
                );

                continue;
            }

            $attributeValueList = $assertionDocument->allDomElementTextContent(
                \sprintf('/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name="%s"]/saml:AttributeValue', $attributeName)
            );

            foreach ($attributeValueList as $attributeValue) {
                if (null !== $processedAttributeValue = $this->processAttributeValue($assertionDocument, $attributeName, $attributeValue, $idpInfo, $spInfo)) {
                    if (!\array_key_exists($attributeName, $attributeList)) {
                        $attributeList[$attributeName] = [];
                        $attributeList[$friendlyName] = [];
                    }
                    $attributeList[$attributeName][] = $processedAttributeValue;
                    $attributeList[$friendlyName][] = $processedAttributeValue;
                }
            }
        }

        return $attributeList;
    }

    /**
     * @param string $attributeName
     * @param string $attributeValue
     *
     * @return string|null
     */
    private function processAttributeValue(XmlDocument $assertionDocument, $attributeName, $attributeValue, IdpInfo $idpInfo, SpInfo $spInfo)
    {
        // eduPersonTargetedID (ePTID)
        if ('urn:oid:1.3.6.1.4.1.5923.1.1.1.10' === $attributeName) {
            // ePTID is a special case as it MAY contain a saml:NameID
            // element and not a simple string value...
            $nameIdDomElement = $assertionDocument->optionalOneDomElement('/saml:Assertion/saml:AttributeStatement/saml:Attribute[@Name="urn:oid:1.3.6.1.4.1.5923.1.1.1.10"]/saml:AttributeValue/saml:NameID');
            if (null !== $nameIdDomElement) {
                $nameId = new NameId($idpInfo->getEntityId(), $spInfo->getEntityId(), $nameIdDomElement);
                if (!$nameId->isPersistent()) {
                    throw new ResponseException('"eduPersonTargetedID" does not have NameID element with Format "persistent"');
                }
                $attributeValue = $nameId->nameIdValue();
            }

            return self::prepareTargetedId($spInfo->targetedIdTemplate(), $idpInfo->getEntityId(), $spInfo->getEntityId(), $attributeValue);
        }

        // filter "scoped" attributes
        // @see https://spaces.at.internet2.edu/display/InCFederation/2016/05/08/Scoped+User+Identifiers
        $idpScopeList = $idpInfo->getScopeList();
        if (0 !== \count($idpScopeList)) {
            $scopedAttributeNameList = [
                'urn:oid:1.3.6.1.4.1.5923.1.1.1.6',  // eduPersonPrincipalName
                'urn:oid:1.3.6.1.4.1.5923.1.1.1.9',  // eduPersonScopedAffiliation
                'urn:oid:1.3.6.1.4.1.5923.1.1.1.13', // eduPersonUniqueId
                // SAML V2.0 Subject Identifier Attributes Profile Version 1.0
                // [SAML-SubjectID-v1.0]
                'urn:oasis:names:tc:SAML:attribute:pairwise-id',
                'urn:oasis:names:tc:SAML:attribute:subject-id',
            ];

            if (\in_array($attributeName, $scopedAttributeNameList, true)) {
                // make sure we have exactly one "@" in the attribute value
                $identifierScopeArray = explode('@', $attributeValue, 2);
                if (2 !== \count($identifierScopeArray)) {
                    $this->logger->warning(
                        \sprintf(
                            'attribute "%s" does not contain a scope "%s"',
                            $attributeName,
                            $attributeValue
                        )
                    );

                    return null;
                }
                // try to match with all IdP supported scopes
                foreach ($idpScopeList as $idpScope) {
                    if ($idpScope === $identifierScopeArray[1]) {
                        return $attributeValue;
                    }
                }
                $this->logger->warning(
                    \sprintf(
                        'attribute "%s" has scoped value "%s" that is not supported by the IdP [%s]',
                        $attributeName,
                        $attributeValue,
                        implode(',', $idpScopeList)
                    )
                );

                return null;
            }

            // schacHomeOrganization
            if ('urn:oid:1.3.6.1.4.1.25178.1.2.9' === $attributeName) {
                // make sure the mentioned domain matches the value(s) of
                // shibmd:Scope *exactly*
                if (!\in_array($attributeValue, $idpScopeList, true)) {
                    return null;
                }
                $this->logger->warning(
                    \sprintf(
                        'attribute "%s" with value "%s" does not use a scope supported by the IdP [%s]',
                        $attributeName,
                        $attributeValue,
                        implode(',', $idpScopeList)
                    )
                );

                return $attributeValue;
            }
        }

        return $attributeValue;
    }

    private static function prepareTargetedId(string $targetedIdTemplate, string $idpEntityId, string $spEntityId, string $userId): string
    {
        return str_replace(
            [
                '{{IDP_ENTITY_ID}}',
                '{{SP_ENTITY_ID}}',
                '{{USER_ID}}',
            ],
            [
                $idpEntityId,
                $spEntityId,
                $userId,
            ],
            $targetedIdTemplate
        );
    }
}
