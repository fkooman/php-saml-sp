<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP;

use fkooman\SAML\SP\Exception\IdpInfoException;
use fkooman\SAML\SP\Exception\KeyException;

/**
 * Holds the configuration information of an IdP.
 */
class IdpInfo
{
    /** @var string */
    private $entityId;

    /** @var array<string,string> */
    private array $displayNames;

    /** @var string */
    private $ssoUrl;

    /** @var string|null */
    private $sloUrl;

    /** @var array<PublicKey> */
    private $publicKeys = [];

    /** @var array<string> */
    private $scopeList = [];

    private bool $hideFromDiscovery;

    /**
     * @param string           $entityId
     * @param array<string,string> $displayNames
     * @param string           $ssoUrl
     * @param string|null      $sloUrl
     * @param array<PublicKey> $publicKeys
     * @param array<string>    $scopeList
     */
    public function __construct($entityId, array $displayNames, $ssoUrl, $sloUrl, array $publicKeys, array $scopeList, bool $hideFromDiscovery = false)
    {
        $this->entityId = $entityId;
        $this->displayNames = $displayNames;
        $this->ssoUrl = $ssoUrl;
        $this->sloUrl = $sloUrl;
        $this->publicKeys = $publicKeys;
        $this->scopeList = $scopeList;
        $this->hideFromDiscovery = $hideFromDiscovery;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    public function getDisplayName(string $preferredLanguage = 'en'): string
    {
        if (0 === \count($this->displayNames)) {
            return $this->entityId;
        }

        // try to find the exact requested language code
        if (\array_key_exists($preferredLanguage, $this->displayNames)) {
            return $this->displayNames[$preferredLanguage];
        }

        // we do not have the exact language code available, try to strip
        // everything after the "-" (inclusive), i.e. "de-DE" becomes "de"
        if (false !== $dashPos = strpos($preferredLanguage, '-')) {
            $preferredLanguage = substr($preferredLanguage, 0, $dashPos);
        }

        // now again try to find the exact requested language code
        if (\array_key_exists($preferredLanguage, $this->displayNames)) {
            return $this->displayNames[$preferredLanguage];
        }

        // we still don't have an exact match, we should at this point start
        // shortening the names in the array keys of the languages we do have
        foreach ($this->displayNames as $k => $v) {
            if (0 === strpos($k, $preferredLanguage)) {
                return $v;
            }
        }

        // we give up, and just return the "first" available language
        return array_values($this->displayNames)[0];
    }

    /**
     * Get SingleSignOn URL.
     *
     * @return string
     */
    public function getSsoUrl()
    {
        return $this->ssoUrl;
    }

    /**
     * Get SingleLogout URL.
     *
     * @return string|null
     */
    public function getSloUrl()
    {
        return $this->sloUrl;
    }

    /**
     * @return array<PublicKey>
     */
    public function getPublicKeys()
    {
        return $this->publicKeys;
    }

    /**
     * @return array<string>
     */
    public function getScopeList()
    {
        return $this->scopeList;
    }

    public function hideFromDiscovery(): bool
    {
        return $this->hideFromDiscovery;
    }

    /**
     * @param string $xmlString
     *
     * @return self
     */
    public static function fromXml($xmlString)
    {
        // we do NOT (again) perform XML schema validation. Entities coming
        // coming from MetadataSource are verified there...
        $xmlDocument = XmlDocument::fromMetadata($xmlString, false);

        return new self(
            self::extractEntityId($xmlDocument),
            self::extractDisplayNames($xmlDocument),
            self::extractSingleSignOnService($xmlDocument),
            self::extractSingleLogoutService($xmlDocument),
            self::extractPublicKeys($xmlDocument),
            self::extractScope($xmlDocument),
            self::extractHideFromDiscovery($xmlDocument)
        );
    }

    /**
     * @return string
     */
    private static function extractEntityId(XmlDocument $xmlDocument)
    {
        return $xmlDocument->requireOneDomAttrValue('/md:EntityDescriptor/@entityID');
    }

    /**
     * @return string
     */
    private static function extractSingleSignOnService(XmlDocument $xmlDocument)
    {
        $ssoLocationList = $xmlDocument->allDomAttrValue('/md:EntityDescriptor/md:IDPSSODescriptor/md:SingleSignOnService[@Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"]/@Location');
        if (0 === \count($ssoLocationList)) {
            throw new IdpInfoException('unable to find SingleSignOnService with HTTP-Redirect binding');
        }

        // for now we only care about the first one...
        return $ssoLocationList[0];
    }

    /**
     * @return string|null
     */
    private static function extractSingleLogoutService(XmlDocument $xmlDocument)
    {
        $sloLocationList = $xmlDocument->allDomAttrValue('/md:EntityDescriptor/md:IDPSSODescriptor/md:SingleLogoutService[@Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"]/@Location');
        if (0 === \count($sloLocationList)) {
            // it is okay if SLO is not supported...
            return null;
        }

        // for now we only care about the first one...
        return $sloLocationList[0];
    }

    /**
     * @suppress PhanUnusedVariableCaughtException
     *
     * @return array<PublicKey>
     */
    private static function extractPublicKeys(XmlDocument $xmlDocument)
    {
        $publicKeyStringList = $xmlDocument->allDomElementTextContent(
            '/md:EntityDescriptor/md:IDPSSODescriptor/md:KeyDescriptor[not(@use) or @use="signing"]/ds:KeyInfo/ds:X509Data/ds:X509Certificate'
        );

        $publicKeyList = [];
        foreach ($publicKeyStringList as $publicKeyString) {
            try {
                $publicKeyList[] = PublicKey::fromEncodedString($publicKeyString);
            } catch (KeyException $e) {
                // we do not (yet) support other keys than RSA at the moment,
                // trying to parse e.g. an EC cert will fail. We ignore those
                // entries..
            }
        }

        return $publicKeyList;
    }

    /**
     * @return array<string>
     */
    private static function extractScope(XmlDocument $xmlDocument)
    {
        return $xmlDocument->allDomElementTextContent(
            '/md:EntityDescriptor/md:IDPSSODescriptor/md:Extensions/shibmd:Scope[not(@regexp) or @regexp="false" or @regexp="0"]'
        );
    }

    /**
     * @return array<string,string>
     */
    private static function extractDisplayNames(XmlDocument $xmlDocument): array
    {
        // we loop over these three XPath queries and take the first one that
        // yields results, they are listed in order of (our) preference...
        $xPathQueryList = [
            '/md:EntityDescriptor/md:IDPSSODescriptor/md:Extensions/mdui:UIInfo/mdui:DisplayName',
            '/md:EntityDescriptor/md:Organization/md:OrganizationDisplayName',
            '/md:EntityDescriptor/md:Organization/md:OrganizationName',
        ];
        foreach ($xPathQueryList as $xPathQuery) {
            $displayNames = [];
            foreach ($xmlDocument->allDomElement($xPathQuery) as $domElement) {
                // the XML schemas make the "xml:lang" attribute REQUIRED, so
                // it will always be there. For static analysis purposes we
                // handle the case it does NOT exist
                if ('' === $xmlLang = $domElement->getAttribute('xml:lang')) {
                    continue;
                }

                if ('' === $nameValue = trim($domElement->textContent)) {
                    continue;
                }

                $displayNames[trim($xmlLang)] = $nameValue;
            }

            // we listed the XPath queries in the order of preference, so if
            // we got values, we use them and ignore the rest...
            if (0 !== \count($displayNames)) {
                return $displayNames;
            }
        }

        // we did not find any usable name to use for this IdP...
        return [];
    }


    private static function extractHideFromDiscovery(XmlDocument $xmlDocument): bool
    {
        foreach ($xmlDocument->allDomElementTextContent('/md:EntityDescriptor/md:Extensions/mdattr:EntityAttributes/saml:Attribute[@Name="http://macedir.org/entity-category"]/saml:AttributeValue') as $attributeValue) {
            if ('http://refeds.org/category/hide-from-discovery' === $attributeValue) {
                return true;
            }
        }

        return false;
    }
}
