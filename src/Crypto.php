<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP;

use fkooman\SAML\SP\Exception\CryptoException;

class Crypto
{
    public const SIGN_OPENSSL_ALGO = \OPENSSL_ALGO_SHA256;
    public const SIGN_ALGO = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
    public const SIGN_DIGEST_ALGO = 'http://www.w3.org/2001/04/xmlenc#sha256';
    public const SIGN_HASH_ALGO = 'sha256';

    public const ENCRYPT_KEY_DIGEST_ALGO = 'http://www.w3.org/2000/09/xmldsig#sha1';
    public const ENCRYPT_KEY_TRANSPORT_ALGO = 'http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p';

    private const ALLOWED_SIGN_ALGO_LIST = [
        'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256' => \OPENSSL_ALGO_SHA256,
        'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384' => \OPENSSL_ALGO_SHA384,
        'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512' => \OPENSSL_ALGO_SHA512,
    ];

    private const ALLOWED_SIGN_DIGEST_ALGO_LIST = [
        'http://www.w3.org/2001/04/xmlenc#sha256' => 'sha256',
        'http://www.w3.org/2001/04/xmlenc#sha384' => 'sha384',
        'http://www.w3.org/2001/04/xmlenc#sha512' => 'sha512',
    ];

    /**
     * @return string
     */
    public static function signXml(XmlDocument $xmlDocument, PrivateKey $privateKey)
    {
        $signatureValueElement = $xmlDocument->requireOneDomElement('self::node()/ds:Signature/ds:SignatureValue');
        $signedInfoElement = $xmlDocument->requireOneDomElement('self::node()/ds:Signature/ds:SignedInfo');
        $digestValueElement = $xmlDocument->requireOneDomElement('self::node()/ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue');

        // in order to generate an XML signature we first need to hash the
        // document, without the ds:Signature element, put that hash inside the
        // DigestValue, then generate a signature over the SignedInfo element
        // and put those values again inside the ds:Signature element of the
        // original document. We have to create a copy of the XML document in
        // order to be able to 'delete' the Signature element without modifying
        // the original document
        $xmlDocumentClone = $xmlDocument->copy();
        $rootElement = $xmlDocumentClone->requireOneDomElement('self::node()');
        $rootElement->removeChild($xmlDocumentClone->requireOneDomElement('self::node()/ds:Signature'));
        $rootElementDigest = Base64::encode(
            hash(
                self::SIGN_HASH_ALGO,
                XmlDocument::canonicalizeElement($rootElement),
                true
            )
        );
        XmlDocument::addTextNode($digestValueElement, $rootElementDigest);
        $signatureValue = Base64::encode(
            self::sign(
                XmlDocument::canonicalizeElement($signedInfoElement),
                $privateKey
            )
        );

        // now we add the signature back inside SignatureValue
        XmlDocument::addTextNode($signatureValueElement, $signatureValue);

        return $xmlDocument->save();
    }

    /**
     * @param array<PublicKey> $publicKeys
     *
     * @throws \fkooman\SAML\SP\Exception\CryptoException
     */
    public static function verifyXml(XmlDocument $xmlDocument, array $publicKeys): void
    {
        $rootElementId = $xmlDocument->requireOneDomAttrValue('self::node()/@ID');
        $referenceUri = $xmlDocument->requireOneDomAttrValue('self::node()/ds:Signature/ds:SignedInfo/ds:Reference/@URI');
        if (\sprintf('#%s', $rootElementId) !== $referenceUri) {
            throw new CryptoException('reference URI does not point to document ID');
        }

        $digestMethod = $xmlDocument->requireOneDomAttrValue('self::node()/ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestMethod/@Algorithm');
        if (!\array_key_exists($digestMethod, self::ALLOWED_SIGN_DIGEST_ALGO_LIST)) {
            throw new CryptoException(
                \sprintf(
                    'digest algorithm "%s" not supported, we only support [%s]',
                    $digestMethod,
                    implode(',', array_keys(self::ALLOWED_SIGN_DIGEST_ALGO_LIST))
                )
            );
        }

        $signatureMethod = $xmlDocument->requireOneDomAttrValue('self::node()/ds:Signature/ds:SignedInfo/ds:SignatureMethod/@Algorithm');
        $signatureValue = self::removeWhiteSpace($xmlDocument->requireOneDomElementTextContent('self::node()/ds:Signature/ds:SignatureValue'));
        $digestValue = self::removeWhiteSpace($xmlDocument->requireOneDomElementTextContent('self::node()/ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue'));
        $signedInfoElement = $xmlDocument->requireOneDomElement('self::node()/ds:Signature/ds:SignedInfo');
        $nsPrefixList = self::extractNsPrefixList($xmlDocument);
        $canonicalSignedInfo = XmlDocument::canonicalizeElement($signedInfoElement);
        $signatureElement = $xmlDocument->requireOneDomElement('self::node()/ds:Signature');
        $rootElement = $xmlDocument->requireOneDomElement('self::node()');
        $rootElement->removeChild($signatureElement);
        $rootElementDigest = Base64::encode(
            hash(
                self::ALLOWED_SIGN_DIGEST_ALGO_LIST[$digestMethod],
                XmlDocument::canonicalizeElement($rootElement, $nsPrefixList),
                true
            )
        );

        // compare the digest from the XML with the actual digest
        if (!hash_equals($rootElementDigest, $digestValue)) {
            throw new CryptoException(\sprintf('unexpected XML digest, we expected "%s", we got "%s"', $rootElementDigest, $digestValue));
        }

        self::verify($canonicalSignedInfo, Base64::decode($signatureValue), $publicKeys, $signatureMethod);
    }

    /**
     * @param string           $inStr
     * @param string           $inSig
     * @param array<PublicKey> $publicKeys
     *
     * @throws \fkooman\SAML\SP\Exception\CryptoException
     */
    public static function verify($inStr, $inSig, array $publicKeys, string $signatureMethod): void
    {
        if (!\array_key_exists($signatureMethod, self::ALLOWED_SIGN_ALGO_LIST)) {
            throw new CryptoException(
                \sprintf(
                    'signature algorithm "%s" not supported, we only support [%s]',
                    $signatureMethod,
                    implode(',', array_keys(self::ALLOWED_SIGN_ALGO_LIST))
                )
            );
        }

        if (0 === \count($publicKeys)) {
            throw new CryptoException('no public key(s) provided');
        }

        // Our XML handling requires the use of a DOMDocument (contained in
        // XmlDocument) that just has the saml:Assertion. This used to work
        // great until we tested with an Azure IdP. It uses the default
        // namespace for the Signature element. Other implementations always
        // use the ds: prefix. This triggers a bug in PHP/libxml where when
        // importing this node into another document, the default namespace is
        // made explicit with the default: prefix. This is very annoying, as
        // this breaks XML signatures...
        //
        // A better solution is probably to NOT import the Assertion in another
        // document and instead do the signature verification directly on the
        // Response document. This works for Assertion, but not
        // EncryptedAssertion. In the latter case the Response XML should be
        // modified and the EncryptedAssertion be replaced by the Assertion.
        // Unfortunately this requires a substantial refactor that I'd like to
        // avoid just after the audit. So instead, we just do simple text
        // replacement to remove the "default" namespace prefix after which the
        // signature verification works again. This is rather safe as if the
        // document is mangled somehow the verification will simply fail. As no
        // other part of the Signature element can possibly contain these
        // strings I'm rather sure this is fine...
        //
        // @see https://bugs.php.net/bug.php?id=55294
        // @see https://bugs.php.net/bug.php?id=47530
        $inStr = str_replace(['<default:', '</default:', 'xmlns:default='], ['<', '</', 'xmlns='], $inStr);

        foreach ($publicKeys as $publicKey) {
            if (1 === openssl_verify($inStr, $inSig, $publicKey->raw(), self::ALLOWED_SIGN_ALGO_LIST[$signatureMethod])) {
                // signature verified
                return;
            }
        }

        throw new CryptoException('unable to verify signature');
    }

    /**
     * @throws \fkooman\SAML\SP\Exception\CryptoException
     *
     * @return string
     */
    public static function decryptXml(XmlDocument $xmlDocument, PrivateKey $privateKey)
    {
        // make sure we support the key transport encryption algorithm
        $keyEncryptionMethod = $xmlDocument->requireOneDomAttrValue('/samlp:Response/saml:EncryptedAssertion/xenc:EncryptedData/ds:KeyInfo/xenc:EncryptedKey/xenc:EncryptionMethod/@Algorithm');
        if (self::ENCRYPT_KEY_TRANSPORT_ALGO !== $keyEncryptionMethod) {
            throw new CryptoException(\sprintf('only key transport algorithm "%s" is supported', self::ENCRYPT_KEY_TRANSPORT_ALGO));
        }

        // make sure we support the key transport encryption digest algorithm
        $keyEncryptionDigestMethod = $xmlDocument->requireOneDomAttrValue('/samlp:Response/saml:EncryptedAssertion/xenc:EncryptedData/ds:KeyInfo/xenc:EncryptedKey/xenc:EncryptionMethod/ds:DigestMethod/@Algorithm');
        if (self::ENCRYPT_KEY_DIGEST_ALGO !== $keyEncryptionDigestMethod) {
            throw new CryptoException(\sprintf('only key encryption digest "%s" is supported', self::ENCRYPT_KEY_DIGEST_ALGO));
        }

        // extract the session key
        $keyCipherValue = self::removeWhiteSpace($xmlDocument->requireOneDomElementTextContent('/samlp:Response/saml:EncryptedAssertion/xenc:EncryptedData/ds:KeyInfo/xenc:EncryptedKey/xenc:CipherData/xenc:CipherValue'));

        // decrypt the session key
        if (false === openssl_private_decrypt(Base64::decode($keyCipherValue), $symmetricEncryptionKey, $privateKey->raw(), \OPENSSL_PKCS1_OAEP_PADDING)) {
            throw new CryptoException('unable to extract session key');
        }

        // find out which encryption algorithm was used for the EncryptedAssertion
        $encryptionMethod = $xmlDocument->requireOneDomAttrValue('/samlp:Response/saml:EncryptedAssertion/xenc:EncryptedData/xenc:EncryptionMethod/@Algorithm');

        // make sure the obtained key is the exact length we expect
        if (CryptoParameters::getKeyLength($encryptionMethod) !== \strlen($symmetricEncryptionKey)) {
            throw new CryptoException('session key has unexpected length');
        }

        // extract the encrypted Assertion
        $assertionCipherValue = Base64::decode(self::removeWhiteSpace($xmlDocument->requireOneDomElementTextContent('/samlp:Response/saml:EncryptedAssertion/xenc:EncryptedData/xenc:CipherData/xenc:CipherValue')));

        // @see https://www.w3.org/TR/xmlenc-core1/#sec-AES-GCM

        // IV (first 96 bits)
        $messageIv = self::expectStringWithLen(
            substr($assertionCipherValue, 0, CryptoParameters::getIvLength($encryptionMethod)),
            CryptoParameters::getIvLength($encryptionMethod)
        );
        // Tag (last 128 bits)
        $messageTag = self::expectStringWithLen(
            substr($assertionCipherValue, \strlen($assertionCipherValue) - CryptoParameters::getTagLength($encryptionMethod)),
            CryptoParameters::getTagLength($encryptionMethod)
        );
        // CipherText (between IV and Tag)
        $cipherText = self::expectStringWithLen(
            substr($assertionCipherValue, CryptoParameters::getIvLength($encryptionMethod), -CryptoParameters::getTagLength($encryptionMethod)),
            \strlen($assertionCipherValue) - CryptoParameters::getIvLength($encryptionMethod) - CryptoParameters::getTagLength($encryptionMethod)
        );
        if (false === $decryptedAssertion = openssl_decrypt($cipherText, CryptoParameters::getOpenSslAlgorithm($encryptionMethod), $symmetricEncryptionKey, \OPENSSL_RAW_DATA, $messageIv, $messageTag)) {
            throw new CryptoException('unable to decrypt data');
        }

        return $decryptedAssertion;
    }

    /**
     * @param string $inStr
     *
     * @throws \fkooman\SAML\SP\Exception\CryptoException
     *
     * @return string
     */
    public static function sign($inStr, PrivateKey $privateKey)
    {
        if (false === openssl_sign($inStr, $outSig, $privateKey->raw(), self::SIGN_OPENSSL_ALGO)) {
            throw new CryptoException('unable to create signature');
        }

        return $outSig;
    }

    /**
     * @return array<string>
     */
    public static function validateNsPrefixList(string $nsPrefixListValue): array
    {
        $nsPrefixList = explode(' ', $nsPrefixListValue);
        foreach ($nsPrefixList as $nsPrefix) {
            // @see https://www.w3.org/TR/xml-names11/#ns-decl
            // we SHOULD be validating [2] `PrefixedAttName` from that
            // document, but we will be more strict, we can always expand
            // supported chars when necessary...
            if (1 !== preg_match('/^[a-zA-Z_]+[a-zA-Z0-9_]*$/', $nsPrefix)) {
                throw new CryptoException(\sprintf('inclusive namespace prefix list contains unsupported chars "%s"', $nsPrefix));
            }
        }

        return $nsPrefixList;
    }

    /**
     * Apparently there is a way to configure Shibboleth IdP to use this
     * "InclusiveNamespaces" transform thingy. Why can't we have nice things?!
     *
     * @return array<string>
     */
    private static function extractNsPrefixList(XmlDocument $xmlDocument): array
    {
        $nsPrefixListValue = $xmlDocument->optionalOneDomAttrValue('self::node()/ds:Signature/ds:SignedInfo/ds:Reference/ds:Transforms/ds:Transform/ec:InclusiveNamespaces/@PrefixList');
        if (null === $nsPrefixListValue) {
            return [];
        }

        return self::validateNsPrefixList($nsPrefixListValue);
    }

    /**
     * @param string $inputStr
     *
     * @return string
     */
    private static function removeWhiteSpace($inputStr)
    {
        // https://www.w3.org/TR/xmlschema11-2/#base64Binary
        // https://www.w3.org/TR/xmlschema11-2/#rf-whiteSpace
        // we do NOT follow the exact rules for white space removal, because
        // they are insane...
        return str_replace(["\x09", "\x0a", "\x0d", "\x20"], '', $inputStr);
    }

    /**
     * @param mixed $inputData
     * @param int   $expectedLength
     *
     * @return string
     */
    private static function expectStringWithLen($inputData, $expectedLength)
    {
        if (!\is_string($inputData)) {
            throw new CryptoException('"string" expected');
        }
        if ($expectedLength !== \strlen($inputData)) {
            throw new CryptoException('unexpected "string" length');
        }

        return $inputData;
    }
}
