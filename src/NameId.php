<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP;

use DOMElement;
use fkooman\SAML\SP\Exception\NameIdException;

class NameId
{
    private ?string $nameIdFormat;
    private ?string $nameQualifier;
    private ?string $spNameQualifier;
    private string $nameIdValue;

    /**
     * @throws \fkooman\SAML\SP\Exception\NameIdException
     */
    public function __construct(string $idpEntityId, string $spEntityId, DOMElement $nameIdElement)
    {
        $nameQualifier = $nameIdElement->hasAttribute('NameQualifier') ? $nameIdElement->getAttribute('NameQualifier') : null;
        if (null !== $nameQualifier) {
            if ($idpEntityId !== $nameQualifier) {
                throw new NameIdException('"NameQualifier" does not match expected IdP entityID');
            }
        }

        $spNameQualifier = $nameIdElement->hasAttribute('SPNameQualifier') ? $nameIdElement->getAttribute('SPNameQualifier') : null;
        if (null !== $spNameQualifier) {
            if ($spEntityId !== $spNameQualifier) {
                throw new NameIdException('"SPNameQualifier" does not match expected SP entityID');
            }
        }

        $this->nameIdFormat = $nameIdElement->hasAttribute('Format') ? $nameIdElement->getAttribute('Format') : null;
        $this->nameQualifier = $nameQualifier;
        $this->spNameQualifier = $spNameQualifier;
        $this->nameIdValue = trim($nameIdElement->textContent);
    }

    /**
     * Get NameID XML string. This SHOULD only be needed for logout (SLO).
     */
    public function toXml(): string
    {
        $attributeList = [];
        if (null !== $this->nameQualifier) {
            $attributeList[] = \sprintf('NameQualifier="%s"', $this->nameQualifier);
        }
        if (null !== $this->spNameQualifier) {
            $attributeList[] = \sprintf('SPNameQualifier="%s"', $this->spNameQualifier);
        }
        if (null !== $this->nameIdFormat) {
            $attributeList[] = \sprintf('Format="%s"', $this->nameIdFormat);
        }

        return \sprintf(
            '<saml:NameID %s>%s</saml:NameID>',
            implode(' ', $attributeList),
            $this->nameIdValue
        );
    }

    public function nameIdFormat(): ?string
    {
        return $this->nameIdFormat;
    }

    public function isPersistent(): bool
    {
        if ('urn:oasis:names:tc:SAML:2.0:nameid-format:persistent' === $this->nameIdFormat) {
            return true;
        }

        if (null === $this->nameIdFormat) {
            // if the Format attribute is not specified, we'll just assume it
            // it is persistent...
            //
            // TODO: we SHOULD remove this, as if NameID is specified it MUST
            // have the persistent Format attribute.
            return true;
        }

        return false;
    }

    public function nameIdValue(): string
    {
        return $this->nameIdValue;
    }
}
