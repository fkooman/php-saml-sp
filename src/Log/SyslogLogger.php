<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Log;

use RuntimeException;

class SyslogLogger implements LoggerInterface
{
    public function __construct(string $ident)
    {
        // in PHP >= 8.2 it always returns true, but in versions < 8.2, it can
        // also return false...
        /** @var bool $logRes */
        $logRes = openlog($ident, \LOG_ODELAY, \LOG_USER);
        if (false === $logRes) {
            throw new RuntimeException('unable to open syslog');
        }
    }

    public function __destruct()
    {
        closelog();
    }

    public function warning(string $logMessage): void
    {
        syslog(\LOG_WARNING, $logMessage);
    }

    public function error(string $logMessage): void
    {
        syslog(\LOG_ERR, $logMessage);
    }

    public function notice(string $logMessage): void
    {
        syslog(\LOG_NOTICE, $logMessage);
    }

    public function debug(string $logMessage): void
    {
        syslog(\LOG_DEBUG, $logMessage);
    }
}
