<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\Web;

use fkooman\SAML\SP\Web\Exception\HttpException;

class Request
{
    /** @var array */
    private $serverData;

    /** @var array<string,string> */
    private array $queryData;

    /** @var array */
    private $postData;

    public function __construct(array $serverData, array $postData)
    {
        $this->serverData = $serverData;
        $this->queryData = self::parseQuery($serverData);
        $this->postData = $postData;
    }

    /**
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->requireHeader('REQUEST_METHOD');
    }

    /**
     * @return string
     */
    public function getRootUri()
    {
        return \sprintf('%s://%s%s', $this->getScheme(), $this->getAuthority(), $this->getRoot());
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return \sprintf('%s://%s%s', $this->getScheme(), $this->getAuthority(), $this->requireHeader('REQUEST_URI'));
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return \sprintf('%s://%s', $this->getScheme(), $this->getAuthority());
    }

    /**
     * @return string
     */
    public function getPathInfo()
    {
        // remove the query string
        $requestUri = $this->requireHeader('REQUEST_URI');
        if (false !== $pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }

        // if requestUri === scriptName
        $scriptName = $this->requireHeader('SCRIPT_NAME');
        if ($requestUri === $scriptName) {
            return '/';
        }

        // remove script_name (if it is part of request_uri
        if (0 === strpos($requestUri, $scriptName)) {
            if (false === $pathInfo = substr($requestUri, \strlen($scriptName))) {
                throw new HttpException(500, 'unable to remove script_name');
            }

            return $pathInfo;
        }

        // remove the root
        if ('/' !== $this->getRoot()) {
            if (false === $pathInfo = substr($requestUri, \strlen($this->getRoot()) - 1)) {
                throw new HttpException(500, 'unable to remove root');
            }

            return $pathInfo;
        }

        return $requestUri;
    }

    /**
     * For SAML HTTP-Redirect binding signature verification we require the
     * "raw" query string.
     *
     * @see saml-bindings-2.0-os (3.4.4.1 DEFLATE Encoding)
     */
    public function requireRawQueryParameter(string $parameterName): string
    {
        if (!\array_key_exists($parameterName, $this->queryData)) {
            throw new HttpException(400, \sprintf('missing query parameter "%s"', $parameterName));
        }

        return $this->queryData[$parameterName];
    }

    /**
     * For SAML HTTP-Redirect binding signature verification we require the
     * "raw" query string.
     *
     * @see saml-bindings-2.0-os (3.4.4.1 DEFLATE Encoding)
     */
    public function optionalRawQueryParameter(string $parameterName): ?string
    {
        if (!\array_key_exists($parameterName, $this->queryData)) {
            return null;
        }

        return $this->queryData[$parameterName];
    }

    public function requireQueryParameter(string $parameterName): string
    {
        return urldecode($this->requireRawQueryParameter($parameterName));
    }

    public function optionalQueryParameter(string $parameterName): ?string
    {
        if (null !== $parameterValue = $this->optionalRawQueryParameter($parameterName)) {
            return urldecode($parameterValue);
        }

        return null;
    }

    /**
     * @param string $postKey
     *
     * @return string
     */
    public function requirePostParameter($postKey)
    {
        if (!\array_key_exists($postKey, $this->postData)) {
            throw new HttpException(400, \sprintf('missing post parameter "%s"', $postKey));
        }
        $postValue = $this->postData[$postKey];
        if (!\is_string($postValue)) {
            throw new HttpException(400, \sprintf('value of post parameter "%s" MUST be string', $postKey));
        }

        return $postValue;
    }

    /**
     * @param string $postKey
     *
     * @return string|null
     */
    public function optionalPostParameter($postKey)
    {
        if (!\array_key_exists($postKey, $this->postData)) {
            return null;
        }

        return $this->requirePostParameter($postKey);
    }

    /**
     * @param string $headerKey
     *
     * @return string
     */
    public function requireHeader($headerKey)
    {
        if (!\array_key_exists($headerKey, $this->serverData)) {
            throw new HttpException(400, \sprintf('missing request header "%s"', $headerKey));
        }

        if (!\is_string($this->serverData[$headerKey])) {
            throw new HttpException(400, \sprintf('value of request header "%s" MUST be string', $headerKey));
        }

        return $this->serverData[$headerKey];
    }

    /**
     * @param string $headerKey
     *
     * @return string|null
     */
    public function optionalHeader($headerKey)
    {
        if (!\array_key_exists($headerKey, $this->serverData)) {
            return null;
        }

        return $this->requireHeader($headerKey);
    }

    /**
     * @return string
     */
    public function getRoot()
    {
        $rootDir = \dirname($this->requireHeader('SCRIPT_NAME'));
        if ('/' !== $rootDir) {
            return \sprintf('%s/', $rootDir);
        }

        return $rootDir;
    }

    /**
     * @return array<string,string>
     */
    private static function parseQuery(array $serverData): array
    {
        if (!\array_key_exists('QUERY_STRING', $serverData)) {
            return [];
        }

        if (!\is_string($serverData['QUERY_STRING'])) {
            return [];
        }

        $queryData = [];
        foreach (explode('&', $serverData['QUERY_STRING']) as $queryElement) {
            if ('' !== $queryElement) {
                if (false === strpos($queryElement, '=')) {
                    $queryData[$queryElement] = '';

                    continue;
                }
                $keyValue = explode('=', $queryElement, 2);
                if (2 !== \count($keyValue)) {
                    continue;
                }
                [$k, $v] = $keyValue;
                $queryData[$k] = $v;
            }
        }

        return $queryData;
    }

    /**
     * @return string
     */
    private function getScheme()
    {
        if (null === $requestScheme = $this->optionalHeader('REQUEST_SCHEME')) {
            $requestScheme = 'http';
        }

        if (!\in_array($requestScheme, ['http', 'https'], true)) {
            throw new HttpException(400, 'unsupported "REQUEST_SCHEME"');
        }

        return $requestScheme;
    }

    /**
     * URI = scheme:[//authority]path[?query][#fragment]
     * authority = [userinfo@]host[:port].
     *
     * @see https://en.wikipedia.org/wiki/Uniform_Resource_Identifier#Generic_syntax
     *
     * @return string
     */
    private function getAuthority()
    {
        // we do not care about "userinfo"...
        $requestScheme = $this->getScheme();
        $serverName = $this->requireHeader('SERVER_NAME');
        $serverPort = (int) $this->requireHeader('SERVER_PORT');

        $usePort = false;
        if ('https' === $requestScheme && 443 !== $serverPort) {
            $usePort = true;
        }
        if ('http' === $requestScheme && 80 !== $serverPort) {
            $usePort = true;
        }

        if ($usePort) {
            return \sprintf('%s:%d', $serverName, $serverPort);
        }

        return $serverName;
    }
}
