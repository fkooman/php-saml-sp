<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\HttpClient;

use fkooman\SAML\SP\HttpClient\Exception\CurlException;
use fkooman\SAML\SP\Log\LoggerInterface;
use fkooman\SAML\SP\Log\NullLogger;

class CurlHttpClient implements HttpClientInterface
{
    private LoggerInterface $logger;

    /** @var resource */
    private $curlChannel;

    private bool $allowInsecureHttp = false;

    /** @var array<string,string> */
    private array $responseHeaderList = [];

    public function __construct(?LoggerInterface $logger = null)
    {
        if (false === $this->curlChannel = curl_init()) {
            throw new CurlException('unable to create cURL channel');
        }
        $this->logger = $logger ?? new NullLogger();
    }

    public function __destruct()
    {
        curl_close($this->curlChannel);
    }

    public function withAllowInsecureHttp(bool $allowInsecureHttp): self
    {
        $self = clone $this;
        $self->allowInsecureHttp = $allowInsecureHttp;

        return $self;
    }

    public function send(Request $request): Response
    {
        $curlOptions = [
            \CURLOPT_CUSTOMREQUEST => $request->getMethod(),
            \CURLOPT_URL => $request->getUri(),
        ];

        if (\in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'], true)) {
            $curlOptions[\CURLOPT_POSTFIELDS] = $request->getBody();
        }

        $response = $this->exec($curlOptions, $request->getHeaders());
        if (!$response->isOkay()) {
            $this->logger->warning(
                \sprintf('REQUEST=%s, RESPONSE=%s', (string) $request, (string) $response)
            );
        }

        $this->logger->debug(
            \sprintf('REQUEST=%s, RESPONSE=%s', (string) $request, (string) $response)
        );

        return $response;
    }

    private function curlReset(): void
    {
        curl_reset($this->curlChannel);
        $this->responseHeaderList = [];
    }

    /**
     * @param array<string,string> $requestHeaders
     */
    private function exec(array $curlOptions, array $requestHeaders): Response
    {
        // make sure we always start with a clean slate, we do this here
        // and not after curl_exec because when calling CurlHttpClient::exec
        // again after a caught exception may result in unexpected weirdness
        $this->curlReset();

        $defaultCurlOptions = [
            \CURLOPT_HEADER => false,
            \CURLOPT_CONNECTTIMEOUT => 5,
            \CURLOPT_TIMEOUT => 10,
            \CURLOPT_RETURNTRANSFER => true,
            \CURLOPT_FOLLOWLOCATION => false,
            \CURLOPT_PROTOCOLS => $this->allowInsecureHttp ? \CURLPROTO_HTTPS | \CURLPROTO_HTTP : \CURLPROTO_HTTPS,
            \CURLOPT_SSL_VERIFYHOST => 2,    // default, but just to make sure
            \CURLOPT_SSL_VERIFYPEER => true, // default, but just to make sure
            \CURLOPT_HEADERFUNCTION => [$this, 'responseHeaderFunction'],
        ];

        if (0 !== \count($requestHeaders)) {
            $curlRequestHeaders = [];
            foreach ($requestHeaders as $k => $v) {
                $curlRequestHeaders[] = \sprintf('%s: %s', $k, $v);
            }
            $defaultCurlOptions[\CURLOPT_HTTPHEADER] = $curlRequestHeaders;
        }

        if (false === curl_setopt_array($this->curlChannel, $curlOptions + $defaultCurlOptions)) {
            throw new CurlException('unable to set cURL options');
        }

        $responseData = curl_exec($this->curlChannel);
        if (false === \is_string($responseData)) {
            // curl_exec returns true/false when CURLOPT_RETURNTRANSFER is not
            // set, but false|string when CURLOPT_RETURNTRANSFER _IS_ set, but
            // Psalm is not clever enough to distinguish this, so if the
            // response is NOT a string it MUST be false
            throw new CurlException(\sprintf('[%d] %s', curl_errno($this->curlChannel), curl_error($this->curlChannel)));
        }

        $httpCode = curl_getinfo($this->curlChannel, \CURLINFO_HTTP_CODE);
        if (!\is_int($httpCode)) {
            throw new CurlException('CURLINFO_HTTP_CODE is not of type `int`');
        }

        return new Response(
            $httpCode,
            $responseData,
            $this->responseHeaderList
        );
    }

    /**
     * @param resource $curlChannel
     */
    private function responseHeaderFunction($curlChannel, string $headerData): int
    {
        // we do NOT support multiple response headers with the same key, the
        // later one(s) will overwrite the earlier one
        if (false !== strpos($headerData, ':')) {
            $keyValue = explode(':', $headerData, 2);
            if (2 !== \count($keyValue)) {
                throw new CurlException('malformed header');
            }
            [$key, $value] = $keyValue;
            $this->responseHeaderList[trim($key)] = trim($value);
        }

        return \strlen($headerData);
    }
}
