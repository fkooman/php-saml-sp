<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP\HttpClient;

use fkooman\SAML\SP\QueryParameters;

class Request
{
    private string $requestMethod;
    private string $requestUri;
    private ?string $requestBody;

    /** @var array<string,string> */
    private array $requestHeaders;

    /**
     * @param array<string,string> $requestHeaders
     */
    public function __construct(string $requestMethod, string $requestUri, array $requestHeaders = [], ?string $requestBody = null)
    {
        $this->requestMethod = $requestMethod;
        $this->requestUri = $requestUri;
        $this->requestBody = $requestBody;
        $this->requestHeaders = $requestHeaders;
    }

    public function __toString(): string
    {
        $requestHeaders = [];
        foreach ($this->requestHeaders as $k => $v) {
            // we do NOT want to log HTTP Basic credentials
            if ('Authorization' === $k) {
                if (0 === strpos($v, 'Basic ')) {
                    $v = 'XXX-REPLACED-FOR-LOG-XXX';
                }
            }
            $requestHeaders[] = \sprintf('%s: %s', $k, $v);
        }

        $requestBody = null === $this->requestBody ? '' : $this->requestBody;

        return \sprintf(
            '[requestMethod=%s, requestUri=%s, requestHeaders=[%s], requestBody=%s]',
            $this->requestMethod,
            $this->requestUri,
            implode(', ', $requestHeaders),
            $requestBody
        );
    }

    /**
     * @param array<string,string> $queryParameters
     * @param array<string,string> $requestHeaders
     */
    public static function get(string $requestUri, array $queryParameters = [], array $requestHeaders = []): Request
    {
        $requestUri = QueryParameters::init($queryParameters)->appendToUrl($requestUri);

        return new self('GET', $requestUri, $requestHeaders);
    }

    /**
     * @param array<string,string> $postData
     * @param array<string,string> $requestHeaders
     */
    public static function post(string $requestUri, array $postData = [], array $requestHeaders = []): Request
    {
        return new self(
            'POST',
            $requestUri,
            array_merge(
                $requestHeaders,
                ['Content-Type' => 'application/x-www-form-urlencoded']
            ),
            (string) QueryParameters::init($postData)
        );
    }

    public function setHeader(string $key, string $value): void
    {
        $this->requestHeaders[$key] = $value;
    }

    public function getMethod(): string
    {
        return $this->requestMethod;
    }

    public function getUri(): string
    {
        return $this->requestUri;
    }

    public function getBody(): ?string
    {
        return $this->requestBody;
    }

    /**
     * @return array<string,string>
     */
    public function getHeaders(): array
    {
        return $this->requestHeaders;
    }
}
