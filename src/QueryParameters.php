<?php

declare(strict_types=1);

/*
 * Copyright (c) 2019-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\SP;

use fkooman\SAML\SP\Exception\QueryParametersException;

class QueryParameters
{
    /** @var array<string,string> */
    private array $queryData = [];

    /**
     * @param array<string,string> $queryData
     */
    public function __construct(array $queryData)
    {
        $this->queryData = $queryData;
    }

    public function __toString(): string
    {
        return http_build_query($this->queryData);
    }

    /**
     * @param array<string,string> $queryData
     */
    public static function init(array $queryData): self
    {
        return new self($queryData);
    }

    public static function fromQueryString(string $queryString): self
    {
        $queryData = [];
        foreach (explode('&', $queryString) as $queryElement) {
            if ('' !== $queryElement) {
                if (false === strpos($queryElement, '=')) {
                    $queryData[$queryElement] = '';

                    continue;
                }
                $keyValue = explode('=', $queryElement, 2);
                if (2 !== \count($keyValue)) {
                    continue;
                }
                [$k, $v] = $keyValue;
                $queryData[$k] = urldecode($v);
            }
        }

        return new self($queryData);
    }

    public function add(string $parameterName, string $parameterValue): void
    {
        $this->queryData[$parameterName] = $parameterValue;
    }

    public function appendToUrl(string $inUrl): string
    {
        if (0 === \count($this->queryData)) {
            // we do not want to add any query parameters
            return $inUrl;
        }
        if (false === strpos($inUrl, '?')) {
            // "inUrl" does not contain any query parameters, just add our
            return $inUrl . '?' . (string) $this;
        }

        $splitUrl = explode('?', $inUrl, 2);
        if (2 !== \count($splitUrl)) {
            throw new QueryParametersException('unable to extract query parameter(s) from "inUrl"');
        }
        [$inUrl, $inUrlQueryString] = $splitUrl;

        // take the existing query parameters, and add our query string to it
        $queryString = self::fromQueryString($inUrlQueryString);
        foreach ($this->queryData as $k => $v) {
            $queryString->add($k, $v);
        }

        return $inUrl . '?' . (string) $queryString;
    }
}
