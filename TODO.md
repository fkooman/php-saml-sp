# TODO

- implementing (SQLite?) caching for eduGAIN purpose with 2k+ IdPs
- validate schema of outgoing SAML messages (`AuthnRequest`, `LogoutRequest`, `Metadata`)?
- `ForceAuthn` in `AuthnRequest` (is anyone actually using this?)
- Implement a way to have multiple SP certificates
    - key rollover?
- add tests for `src/Web` and `src/Api` classes
- add keyword support for the WAYF search function
- expose requested attributes through SP metadata?
- Improve SLO?
    - Implement unsolicited `Response`, "IdP initiated"
    - Receive unsolicited `LogoutRequest` from IdPs
