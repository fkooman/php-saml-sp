# FAQ 

## Why do attributes my IdP sends not show up at the SP?

The attribute is filtered because:

1. It is not in the `urn:oid` format, or not in the 
   [list](https://codeberg.org/fkooman/php-saml-sp/src/branch/main/src/AttributeMapping.php)
   of supported attributes;
2. The _scope_ of the attribute, i.e. the part behind the `@` of an attribute 
   value, for example `eduPersonPrincipalName`, is not listed in the 
   `<shibmd:Scope>` element of the IdP metadata.

Problem (1) you can fix with a "custom" override. Put a JSON file in 
`/etc/php-saml-sp/attribute_mapping.json` that converts the received attribute
in its "FriendlyName", e.g.:

```json
{
    "urn:mace:surf.nl:attribute-def:surf-autorisaties": "surf-autorisaties"
}
```

This will make the attribute available, both as 
`urn:mace:surf.nl:attribute-def:surf-autorisaties` as well as 
`surf-autorisaties`. 

If you need to do this for internationally standarized attributes, please 
create an [issue](https://codeberg.org/fkooman/php-saml-sp/issues/new) and 
refer us to that particular attribute registry so we can add them to the 
default attribute registry.

## Why does eduPersonTargetedID look so strange?

The default value for `eduPersonTargetedID` is "serialized" including the 
entity IDs of the IdP and SP, e.g. 
`https://idp.example.org/saml!https://sp.example.com/saml!5Poe7wINn4MQ9G`. 
Binding the user's identity to both the IdP and SP makes sense as the 
identifier is (persistently) unique for every IdP/SP combination.

However, if you want, you can change the "template" to not include these 
details. We have a setting in `/etc/php-saml-sp/config.php` called 
`targetedIdTemplate` that you can set. Examples:

```php
// default ("Shibboleth Style")
'targetedIdTemplate' => '{{IDP_ENTITY_ID}}!{{SP_ENTITY_ID}}!{{USER_ID}}',

// only the "User ID"
'targetedIdTemplate' => '{{USER_ID}}',
```

## How can I use my own SAML SP certificate(s) and key(s)?

Certificates for signing and encryption are created during the installation
process. In case you want to use your own certificates (for signing and
encryption) replace certificates keys in `/etc/ssl/php-saml-sp` on Debian /
Ubuntu, and `/etc/pki/php-saml-sp` on Fedora / EL. Make sure the permissions
are the same as the ones of the generated files, see below:

### Debian / Ubuntu

```bash
# find /etc/ssl/php-saml-sp -printf "perm="%m\ "user="%u\ "group="%g\ "name="%p""\\n
perm=750 user=root group=www-data name=/etc/ssl/php-saml-sp
perm=755 user=root group=root name=/etc/ssl/php-saml-sp/private
perm=640 user=root group=www-data name=/etc/ssl/php-saml-sp/private/signing.key
perm=640 user=root group=www-data name=/etc/ssl/php-saml-sp/private/encryption.key
perm=640 user=root group=www-data name=/etc/ssl/php-saml-sp/encryption.crt
perm=640 user=root group=www-data name=/etc/ssl/php-saml-sp/signing.crt
```

### Fedora / EL

```bash
# find /etc/pki/php-saml-sp -printf "perm="%m\ "user="%u\ "group="%g\ "name="%p""\\n
perm=750 user=root group=apache name=/etc/pki/php-saml-sp
perm=750 user=root group=apache name=/etc/pki/php-saml-sp/private
perm=640 user=root group=apache name=/etc/pki/php-saml-sp/private/encryption.key
perm=640 user=root group=apache name=/etc/pki/php-saml-sp/private/signing.key
perm=640 user=root group=apache name=/etc/pki/php-saml-sp/encryption.crt
perm=640 user=root group=apache name=/etc/pki/php-saml-sp/signing.crt
```
