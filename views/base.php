<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\SAML\SP\Web\Tpl $this */ ?>
<?php /** @var string $requestRoot */ ?>
<?php /** @var string $serviceName */ ?>
<?php /** @var array<string> $enabledLanguages */ ?>
<?php /** @var string $languageCode */ ?>
<!DOCTYPE html>

<html lang="<?=$this->e($languageCode);?>" dir="<?=$this->textDir(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$this->e($serviceName); ?></title>
    <link href="<?=$this->getAssetUrl($requestRoot, 'css/screen.css'); ?>" media="screen" rel="stylesheet">
    <script src="<?=$this->getAssetUrl($requestRoot, 'js/search.js'); ?>"></script>
</head>
<body>
    <header>
<?php if (1 < \count($enabledLanguages)): ?>
        <form method="post" id="langSwitch" action="setLanguage">
            <label>🌐
                <select name="uiLanguage">
<?php foreach ($enabledLanguages as $uiLanguage): ?>
<?php if ($uiLanguage === $languageCode): ?>
                    <option selected value="<?=$this->e($uiLanguage); ?>"><?=$this->languageCodeToHuman($uiLanguage); ?></option>
<?php else: ?>
                    <option value="<?=$this->e($uiLanguage); ?>"><?=$this->languageCodeToHuman($uiLanguage); ?></option>
<?php endif; ?>
<?php endforeach; ?>
                </select>
            </label>
            <button type="submit"><?=$this->t('Switch');?></button>
        </form>
        <script src="<?=$this->getAssetUrl($requestRoot, 'js/language-switcher.js'); ?>" defer></script>
<?php endif; ?>
    </header>
    <main>
        <?=$this->section('content'); ?>
    </main>
    <footer>
        Powered by <a href="https://www.php-saml-sp.eu/">php-saml-sp</a>
    </footer>
</body>
</html>
